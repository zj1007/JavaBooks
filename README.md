<h1 align="center">Java程序员必读书单（超1000本PDF，附下载地址）</h1>
<p align="center">
<a href="https://mp.weixin.qq.com/s/2-PqF7aYCihbJk-fY8xrOg"><img src="https://img.shields.io/badge/公众号-沉默王二-green.svg?style=for-the-badge" alt="公众号"></a>
  <a href="https://www.zhihu.com/people/cmower"><img src="https://img.shields.io/badge/知乎-沉默王二-informational.svg?style=for-the-badge" alt="知乎"></a>
   <a href="https://blog.csdn.net/qing_gee"><img src="https://img.shields.io/badge/CSDN-沉默王二-important.svg?style=for-the-badge" alt="CSDN"></a>
       <a href="https://space.bilibili.com/513340480"><img src="https://img.shields.io/badge/哔哩哔哩-沉默王二-9cf?style=for-the-badge" alt="哔哩哔哩"></a>
    <a href="https://github.com/itwanger/toBeBetterJavaer"><img src="https://img.shields.io/badge/GitHub-Java程序员进阶之路-critical.svg?style=for-the-badge" alt="Java程序员进阶之路">   
           <a href="https://github.com/itwanger/JavaBooks"><img src="https://img.shields.io/badge/PDF-免费计算机书籍-lightgrey.svg?style=for-the-badge" alt="免费PDF"></a>
</p>


#### 1、👉 如果国内访问Github网速较慢，我在码云也放了一份书籍资源，国内访问速度更快，<a href="https://gitee.com/itwanger/JavaBooks" target="_blank">点此直达 </a>

本仓库**持续更新**中，后续会陆续分享更多经典电子书，**强烈建议大家 Star 下本仓库**，下次找书可以直接 **Ctrl + F**，找书再也不愁 ！ 


#### 2、🔥🔥🔥Java程序员进阶之路

我把**自己近 10 年来学习编程的所有原创文章和学习资料**做成了一个网站，适用于**计算机校招应届生以及毕业三年之内的社招求职党**，<a href="https://tobebetterjavaer.com/" target="_blank">传送门</a>

<div align="center">
  <a href="https://tobebetterjavaer.com/home.html">
  <img src="http://cdn.tobebetterjavaer.com/tobebetterjavaer/images/tobebetterjavaer-map.png" target="_blank">
  </a>
</div>

  

#### 3、👍🏻一位美团后端研发工程师的面渣逆袭手册，<a href="https://tobebetterjavaer.com/sidebar/sanfene/nixi.html" target="_blank">点此一键免费获取</a> 

#### 4、⭐可能是2022年全网最全的学习和找工作的PDF资源，<a href="https://tobebetterjavaer.com/pdf/" target="_blank">点此一键免费获取</a> 

#### 5、😜发现一个相当不错的计算机各类种语言&学科学习路线， <a href="https://tobebetterjavaer.com/xuexiluxian/" target="_blank">点此查看</a>

#### 6、赞赏

<div align=left>
    <p>这些书籍基本都是我从一个盗版电子书网站上收集到的，网址是：kanshuy1234.com，现在分享出来希望能对大家有所帮助，自己也花了很久时间整理出来的。<br><br>
      如果觉得本仓库有用，赏赐一块钱，买杯奶茶喝可好？感谢您了~</p>
<figure class="half" align="center">
    <img src="http://cdn.tobebetterjavaer.com/tobebetterjavaer/images/weixin-zhifu.png" width="260px" />
</figure></div>


>点击下列目录直接跳转书籍所在类别，但有时目录跳转会失灵...如果没有没有正常跳转可以动动小手向下拉即可，全部的书籍都在本页面。 
>
>笔者是一个人在维护这个仓库，本地大概是1100多本书了，需要一步步慢慢上传，我只能在闲暇时间慢慢更新，目前已经更新超过600+了，如果没有您要用的书，可以加一下 **个人微信**([qing_gee](http://cdn.tobebetterjavaer.com/tobebetterjavaer/images/qing_gee_noname.jpg))，注明来意，我会慢慢添加上去的。一个人的力量是有限的，请谅解一下。

- [01、入门](#入门)
- [02、工具](#工具)
- [03、框架](#框架)
  - [Struts2](#Struts2)
  - [Spring](#Spring)
  - [Netty](#Netty)
- [04、数据库](#10、数据库)
  - [SQL](#SQL)
  - [MySQL](#MySQL)
  - [Redis](#Redis)
  - [MongoDB](#MongoDB)
- [05、并发编程](#并发编程)
- [06、JVM](#JVM)
- [07、性能优化](#性能优化)
- [08、设计模式](#设计模式)
- [09、操作系统](#操作系统)
  - [Linux 基础知识](#Linux基础知识)
  - [Linux 环境编程](#Linux环境编程)
  - [Linux 内核](#Linux内核)
- [10、计算机网络](#计算机网络)
  - [Linux 网络编程](#Linux网络编程)
  - [wireshark](#wireshark)
- [11、数据结构与算法](#数据结构与算法)
- [12、面试](#面试)
- [13、大数据](#大数据)
- [14、架构](#架构)
- [15、管理](#管理)
- [16、扩展](#扩展)
  - [领域驱动设计](#DDD)
  - [区块链](#区块链)
  - [人工智能](#人工智能)
  - [搜索引擎](#搜索引擎)
  - [网络安全](#网络安全)
  - [消息队列](#消息队列)
  - [云计算](#云计算)
  - [AR&VR](#AR&VR)
  - [Docker](#Docker)
  - [Kubernets](#Kubernets)
  - [IoT](#IoT)
  - [测试](#测试)
  - [其他语言](#其他语言)
    - [Android](#Android)
    - [C](#C)
    - [C++](#C++)
    - [JavaScript](#JavaScript)
    - [Python](#Python)
    - [go](#go)
    - [JavaWeb](#JavaWeb)
    - [JSP](#JSP)
    - [Kotlin](#Kotlin)
- [17、加餐](#加餐)
- [18、活着](#活着)
- [免责声明](#免责声明)


简单说一下我为什么要花半个多月的时间来整理这份书单。主要是因为很多读者的知识体系是零散的，不成系统的，况且技术书籍这么庞杂。有了我这份清单之后，那些没有经验或者经验有限的初学者，在学习的时候思路瞬间就开阔了许多：少走弯路，利用有限的精力，更加高效地学习。

- 想应聘初级 Java 工程师，那只需要阅读入门、工具、框架和数据库方面的书籍就行了；
- 如果想应聘 Java 高级工程师，那么就需要阅读并发编程、底层、性能优化方面的书籍；
- 如果还想更进一步，那么就要着手阅读设计模式、操作系统、计算机网络、数据结构与算法等方面的书籍；
- 记住一点，在应聘之前，请恶补一下面试方面的资料；
- 如果时间充沛，大数据、架构、管理方面的书籍可以读起来；
- 如果还有时间，DDD、区块链、人工智能、搜索引擎、网络安全、消息队列、云计算、容器、智能家居等等方面的书籍，就可以读起来了；

技术学累了，可以读一读理财金融方面的书籍，比如说香帅北大金融学课、李笑来的学习学习再学习，思维认知方面，强烈推荐《沉默的大多数》，我的偶像王小波的散文集。

最后，不管怎样，活着最重要！


## 入门

- Java 程序员进阶之路       [百度云下载链接](https://pan.baidu.com/s/1UkyKSmQ_oabpY6HJZyl7pw)  密码:v0i5
- GitHub 上标星 115k+ 的 Java 教程       [百度云下载链接](https://pan.baidu.com/s/1rT0l5ynzAQLF--efyRHzQw)  密码:dz95
- Java 核心知识点整理       [百度云下载链接](https://pan.baidu.com/s/1AkY43NQeejg4SON8PtSnOg) 提取码:e6tl 
- Java 基础核心总结       [百度云下载链接](https://pan.baidu.com/s/1vFDVc214I00m3VGbZkHxLA) 提取码:x2qi 
- Java 软件开发复习提纲       [百度云下载链接](https://pan.baidu.com/s/1VDewdGcBg7_cNgY-7t0lqA) 提取码:ztfu 
- Java 技术手册       [百度云下载链接](https://pan.baidu.com/s/1KYNE4yjt1DiizuZ7Fjl0Cw) 提取码:wx6l 

## 工具

- Maven 入门指南松哥版       [百度云下载链接](https://pan.baidu.com/s/1BF7JMjLdpwXUkP-ckRsTRw)  密码:bztw
- Eclipse 插件开发学习笔记       [百度云下载链接](https://pan.baidu.com/s/1QmnVfn8iAR0ZnymnFHHTYw)  密码:ri78
- 日志系统手册（Log4j、SLF4J、Logback、Log4j 2）       [百度云下载链接](https://pan.baidu.com/s/1dPwsQhT5OMVapE7hGi7vww)  密码:fxxy
- IntelliJ IDEA 简体中文专题教程（电子版-2015）       [百度云下载链接](https://pan.baidu.com/s/1zyTQ_clx-lLFhjHvakJYBQ)  密码:wskm

## 框架

- SpringMVC 入门指南松哥版       [百度云下载链接](https://pan.baidu.com/s/1gb9biSZoIXh3Au-kT5pkrw)  密码:1j0h
- MyBatis 从入门到精通       [百度云下载链接](https://pan.baidu.com/s/1cYpBIyfJYR4-J8aNNG9MZQ)  密码:8vjv
- MyBatis 入门指南松哥版       [百度云下载链接](https://pan.baidu.com/s/1I6wIKTpLJRmZrcAUdw0B-g)  密码:7zj7

### Struts2

- 开发技巧和整合策略-Struts2       [百度云下载链接](https://pan.baidu.com/s/1f_iot1ryVTNJvhK6lDod5Q)  密码:htn3

### Spring

- Spring 知识点概述       [百度云下载链接](https://pan.baidu.com/s/1u5b3BdTyVBAB1mgqzVp7Ow)  密码:1hcq
- Spring 入门指南松哥版       [百度云下载链接](https://pan.baidu.com/s/17nzGQQf5mD7i-WEHxXWLsg)  密码:zvob
- Spring 技术手册       [百度云下载链接](https://pan.baidu.com/s/1XEKhXPJ03QO7ZELb1Zj84A)  密码:ox17

### Netty

- Netty 进阶之路 跟着案例学       [百度云下载链接](https://pan.baidu.com/s/1jg4BGtw8kFrH7nKBIo0TtA)  密码:iwij



## 数据库

- 数据库系统基础教程       [百度云下载链接](https://pan.baidu.com/s/10aDnMcJiFooUEssYajBUmQ)  密码:nmee
- 自己动手设计数据库       [百度云下载链接](https://pan.baidu.com/s/1ab0dwoxmpHU7BYPuDFLnSA)  密码:tj8g
- SQL+Server+2008 实战       [百度云下载链接](https://pan.baidu.com/s/1jFssmyZd0jm7-v4N3VjIoQ)  密码:5m2v

### SQL

- SQL 必知必会       [百度云下载链接](https://pan.baidu.com/s/1lLl6teMBCUl8SCXLnfuSMA)  密码:qv4z

### MySQL

- 深入浅出MySQL       [百度云下载链接](https://pan.baidu.com/s/1jwBTcbMKMuTOeWXR44v-mQ)  密码:ri07


### Redis

- Redis 入门指南松哥版       [百度云下载链接](https://pan.baidu.com/s/1upM56tYlfwW3vQOVDg-qAA)  密码:iuj9

### MongoDB

- MongoDB实战(第二版)       [百度云下载链接](https://pan.baidu.com/s/1M2kPlol1eBE6E2vvqvGl1Q)  密码:bhxe



## 并发编程

- 深入浅出 Java 多线程       [百度云下载链接](https://pan.baidu.com/s/11Z-IfAPEZNFWp_mAtqDIKw)  密码:drjx

## JVM

- 深入理解 Java 虚拟机总结       [百度云下载链接](https://pan.baidu.com/s/12rhEuLhv_9wXha1Ehm6Tgg)  密码:ixev
- 深入理解 Java 内存模型       [百度云下载链接](https://pan.baidu.com/s/19LNAAX9S282D0W0tt-cbMQ)  密码:k3c6
- Java JDK 学习笔记       [百度云下载链接](https://pan.baidu.com/s/1g-FsMSsPipStu_XIszLLVQ)  密码:9o05


## 性能优化

- 嵩山版阿里巴巴 Java 开发手册       [百度云下载链接](https://pan.baidu.com/s/1iBVFWUPuJNFEBfG8cmd-aA)  密码:pplh

## 设计模式

- 深入浅出设计模式       [百度云下载链接](https://pan.baidu.com/s/1BEkq5XVOwEIP2MEuv4yh6w)  密码:yuvv

## 操作系统

- 操作系统核心知识点       [百度云下载链接](https://pan.baidu.com/s/13bKplAS_7q0LwqpE7C4l6w)  密码:3rp1
- 给操作系统捋条线       [百度云下载链接](https://pan.baidu.com/s/1aNO_diSH29zpyEWO6wr4ag)  密码:eek5

### Linux基础知识

- 鸟哥的 Linux 私房菜       [百度云下载链接](https://pan.baidu.com/s/1bRk0exP_8sVGwSGaZ8k8rw)  密码:yzsl


### Linux环境编程

- Linux-Unix 系统编程手册       [百度云下载链接](https://pan.baidu.com/s/1WC2CuSMtozOljYGw9g6YJQ)  密码:7i9n

### Linux内核

-  Linux内核完全注释(附linux0.11内核源码,超全注释)       [百度云下载链接](https://pan.baidu.com/s/1LSM5C2ANvzj45aEHaXCg3Q)  密码:4azv

## 计算机网络

- 计算机网络-自顶向下方法       [百度云下载链接](https://pan.baidu.com/s/1wDRlqZgo_IUEai5RjUKx9A)  密码:d3tj

### Linux网络编程

- Linux 多线程服务端编程       [百度云下载链接](https://pan.baidu.com/s/1I8kSa7h8_5ws5A40Op4ogw)  密码:2rp4

### wireshark

- Wireshark数据包分析实战       [百度云下载链接](https://pan.baidu.com/s/1cit9gebAAGdPYUiu7ufJSA)  密码:by6w

## 数据结构与算法


- 数据结构与算法分析-Java 描述       [百度云下载链接](https://pan.baidu.com/s/1eTCJ8U6xlBcRK8h1snSVCA)  密码:b0l2
- Java 常用算法       [百度云下载链接](https://pan.baidu.com/s/1NUvhGjiNW28X27-qXCM9Xg)  密码:ybvr
- BAT LeetCode 刷题手册       [百度云下载链接](https://pan.baidu.com/s/12RT8pRk6OUNa1PuYkqZliw)  密码:8w3m


## 面试

- 2020年字节跳动Java 工程师面试题       [百度云下载链接](https://pan.baidu.com/s/1i0YAOKxUl59wP_9yKbeOKA)  密码:iozq
- Google 师兄的刷题笔记       [百度云下载链接](https://pan.baidu.com/s/1ojBerkBfgMFpYcj-JfDKlw)  密码:5ttz
- BAT面试常问80题       [百度云下载链接](https://pan.baidu.com/s/1l7UnWRdPwoQEINhHUmOmFw)  密码:c54x
- 一线互联网企业面试题       [百度云下载链接](https://pan.baidu.com/s/11Nn8dLzh4npR02FWZSGGbA)  密码:wjrr
- 编程之美       [百度云下载链接](https://pan.baidu.com/s/1sMG_Kp66XVRidLNFNd2Znw)  密码:ng5q
- 程序员面试宝典       [百度云下载链接](https://pan.baidu.com/s/1fFVY_-grQjClqnI22QQXDA)  密码:6rr8
- 剑指Offer：名企面试官精讲典型编程题       [百度云下载链接](https://pan.baidu.com/s/14knPPFXiEmxivaS3g0V86w)  密码:lbsn
- 程序员代码面试指南 IT名企算法与数据结构题目最优解       [百度云下载链接](https://pan.baidu.com/s/1s1EA_hrzSnVsQYos4N_cYA)  密码:0djm
- 如何刷力扣       [百度云下载链接](https://pan.baidu.com/s/1q9n68HzyjoqnUBnMZxCDQg)  密码:h14s
- 力扣最优解       [百度云下载链接](https://pan.baidu.com/s/1MQwORt4unKtudXXAOxZt-Q)  密码:o28k
- 2020最新Java面试题资料       [百度云下载链接](https://pan.baidu.com/s/1jO64IiD71gSJcTZmSV3GOA)  密码:mlf8
- 最新Java程序员面试宝典       [百度云下载链接](https://pan.baidu.com/s/1hBU4pIDThFoqjvQY7HnB3g)  密码:u5gh
- JavaGuide 面试突击       [百度云下载链接](https://pan.baidu.com/s/1-vv1FaEIuZCxsz-oemZ91w)  密码:e0p4
- Java 核心面试知识整理       [百度云下载链接](https://pan.baidu.com/s/1OM0axrB6QFG0bJHbIG6_wA)  密码:387r
- Java面试题以及答案       [百度云下载链接](https://pan.baidu.com/s/1W_1c7Jr2sdsHh3khJ2BCvg)  密码:y4ef
- 面试必问之jvm与性能优化       [百度云下载链接](https://pan.baidu.com/s/1M5w3XrffI4GtK_NBRdJnaQ)  密码:y6iu
- Spring 面试题       [百度云下载链接](https://pan.baidu.com/s/15LVGuBYEkPZH9vvP-CQ48Q)  密码:77ud
- Dubbo 面试题       [百度云下载链接](https://pan.baidu.com/s/1Pe8VB8nQz54Los6L_OP6dg)  密码:cl5a
- 简历模板与优化       [百度云下载链接](https://pan.baidu.com/s/1YtMtFXY6CJjVaajSiV58SA)  密码:1cb0

## 大数据

- 大数据入门指南       [百度云下载链接](https://pan.baidu.com/s/1lu0hEPCsiSdy8rFeqx6sWQ)  密码:tk7a

## 架构

- 大型网站技术架构 核心原理与案例分析       [百度云下载链接](https://pan.baidu.com/s/1BG7ZwCsRWyE2itSYUKcDRA)  密码:r5k1


## 扩展

### 其他语言

#### C

- 阮一峰 C语言入门教程       [百度云下载链接](https://pan.baidu.com/s/1TjkEdBNjSO-NtWUxa4zeGg) 提取码:epkk



#### C++


- 牛客校招面试题（附答案与解析）c++篇       [百度云下载链接](https://pan.baidu.com/s/18B9DHuYZPgj2mgLb4ETqKA)  密码:h7im
- C++ 面试题库       [百度云下载链接](https://pan.baidu.com/s/1fBLDu3sOw3qaQuWMblac4w)  密码:qhrg

#### JavaScript

- JavaScript王者归来       [百度云下载链接](https://pan.baidu.com/s/1tr2WtDy55UkOSNzPymh2hA)  密码:xz1j


#### Python

- 流畅的 Python       [百度云下载链接](https://pan.baidu.com/s/1_HZqWh3niD0HodS7xw1imA)  密码:ssjd

#### go

- 学习 go 语言       [百度云下载链接](https://pan.baidu.com/s/1m_3CQ7Jm1yQ6c8ritXYerA)  密码:grvq

#### Android

- 第一行代码 Android  [百度云下载链接](https://pan.baidu.com/s/1Wz30fINgux3wJvyZ3AAZAg)  密码: l5nt

#### JavaWeb

- JAVA EE WEB开发实例精解       [百度云下载链接](https://pan.baidu.com/s/1znvbo645zZXHmTtrbDtD3g)  密码:m7l0

#### JSP

- JSP 程序设计       [百度云下载链接](https://pan.baidu.com/s/1ANZa0Ng0wOgSnYd1HDj1HA)  密码:o8im

#### Kotlin

- kotlin-in-chinese       [百度云下载链接](https://pan.baidu.com/s/1hrfmM1kJqfIhpDV7M8I7vA)  密码:53om

#### groovy

- groovy 程序设计       [百度云下载链接](https://pan.baidu.com/s/1qTrGCLHaBCTUxLUIVP3BpA)  密码:xmjl

### DDD

- 领域驱动设计精简版       [百度云下载链接](https://pan.baidu.com/s/1YnPwZMmcZfD9n7Vx7L3oYA)  密码:9e3x

### 区块链
### 人工智能
- 机器学习与实战       [百度云下载链接](https://pan.baidu.com/s/1O9q1c7pODoZjZcV1w5QjSg)  密码:buvz

### 搜索引擎

- Elasticsearch 技术解析与实战       [百度云下载链接](https://pan.baidu.com/s/1zFCit5IE7egyC8ArWNdFwQ)  密码:yg98

### 网络安全

- Java 加密与解密的艺术       [百度云下载链接](https://pan.baidu.com/s/1H_E4qXeRZ7Cg73_avZfUxw)  密码:1kgn

### 消息队列

- RabbitMQ实战 高效部署分布式消息队列       [百度云下载链接](https://pan.baidu.com/s/1CSSMuIh7ZP7XdcmexAIQJA)  密码:26s7

### 云计算

- 大话云计算       [百度云下载链接](https://pan.baidu.com/s/1p4t9IftbE3BwmQoucKHnjQ)  密码:efwj

### AR&VR
### Docker

- Docker入门指南松哥版       [百度云下载链接](https://pan.baidu.com/s/1DNNAi11bfoKVWogjnnKuIg)  密码:q175

### IoT
### Kubernets

- KUBERNETES权威指南  从DOCKET到KURBERNETES实践全接触       [百度云下载链接](https://pan.baidu.com/s/19POddjLvy6PaADvaC1yLYg)  密码:njo1

### 测试

- 有效的单元测试       [百度云下载链接](https://pan.baidu.com/s/1nIxtJNYpYIGBJY_RXNLx4g)  密码:kbc4


## 管理

- 微管理：给你一个技术团队，你该怎么管（全彩）       [百度云下载链接](https://pan.baidu.com/s/1QurLZsDK8SPHvWCGjgt0sA)  密码:u6ml

## 加餐


- 《阿里技术参考图册》（算法篇）       [百度云下载链接](https://pan.baidu.com/s/1DdT84xRPa4SHmH2SQoRZ6w)  密码:4eev
- 《阿里技术参考图册》（研发篇）       [百度云下载链接](https://pan.baidu.com/s/1xLJ2ArI93gCRa6b0cPBkrQ)  密码:hq1u
- 程序员必知的硬核知识大全       [百度云下载链接](https://pan.baidu.com/s/1yCCvpQONyrwoSGsDlr1FWA)  密码:dp1p
- how-to-be-a-programmer-cn       [百度云下载链接](https://pan.baidu.com/s/1iXTKzEH9B2JXHgWpC30cvA)  密码:kar9

## 活着

- 程序员健康指南       [百度云下载链接](https://pan.baidu.com/s/1EssOkFfZV93QIB9IAFmjmw)  密码:pl0i
- 颈椎康复指南       [百度云下载链接](https://pan.baidu.com/s/1AdqcGTLOUkQxrFFURNYq7A)  密码:ouhh
- 刷爆朋友圈的互联网公司作息表格       [百度云下载链接](https://pan.baidu.com/s/1r7kdeKx8_nq2kASOVnGRGQ)  密码:ssuy




## 免责声明

本仓库书籍链接全部来源于网络其他人的整理的链接，个人只是搜录整理他人成果。

如有疑问请提交**issue**，有**违规侵权**，请联系本人 **www.qing_gee@163.com** ，本人立马删除相应链接，感谢！

本仓库仅作学习交流分享使用，不作任何商用。
